-- Local Helpers --
local function half_equals(t1, t2)
	for k, v in pairs(t1) do
		if t2[k] == nil then
			return false
		elseif type(v) == "table" then
			if (v ~= t1) and (not half_equals(t2[k], v)) then
				return false
			end
		elseif not (t2[k] == v) then
			return false
		end
	end
	return true
end

-- Global Helpers --
function table.equals(t1, t2)
	return half_equals(t1, t2) and half_equals(t2, t1)
end

function table.merge(t, ...)
	for i = 1, select("#", ...) do
		for k, v in pairs(select(i, ...)) do
			t[k] = v
		end
	end

	return t
end

function table.map(t, f)
	for i, v in ipairs(t) do
		t[i] = f(v)
	end

	return t
end

-- Tests --
local error_message = "Function %s does not pass self-test!"

do
	-- table.equals --
	local error = error_message:format("table.equals")

	local test_table = { "a", "b", c = false, e = { "f", "g", "h", { "h" } } }
	table.insert(test_table, test_table)

	local test1 = table.equals(test_table, test_table)
	assert(test1, error)

	local test2 = not table.equals({ a = 1 }, { a = 2 })
	assert(test2, error)
end

do
	-- table.merge --
	local error = error_message:format("table.merge")

	local function test_table_a()
		return { "a", 2, c = 3 }
	end

	local function test_table_b()
		return { "b" }
	end

	local function test_table_c()
		return { c = 4 }
	end

	local test1 = table.merge(test_table_a(), test_table_b())
	assert(table.equals(test1, { "b", 2, c = 3 }), error)

	local test2 = table.merge(test_table_a(), test_table_b(), test_table_c())
	assert(table.equals(test2, { "b", 2, c = 4 }), error)

	local test3 = table.merge(test_table_a())
	assert(table.equals(test3, { "a", 2, c = 3 }), error)
end

do
	-- table.map --
	local error = error_message:format("table.map")

	local function add_one(x)
		return x + 1
	end

	local test_table = { 4, 5, 6 }

	local test1 = table.map(test_table, add_one)
	assert(table.equals(test_table, { 5, 6, 7 }), error)
end

return true
