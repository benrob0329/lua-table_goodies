# table_goodies
This is a small package for extra (can't live without) functions for working with tables.

This includes:

* `table.equals(t1, t2)` -> `bool`, a recursive (deep) equality function
* `table.merge(t, ...)` -> `t`, a table merging function (select("#", ...) -> select(1, ...) -> t1)
* `table.map(t, f)` -> `t`, a mapping (apply f to every value) function

## Installation
### Luarocks
```Bash
luarocks install table_goodies
```

### Minetest
This module is available on Minetest's [ContentDB](https://content.minetest.net/), simple add `table_goodies` as a dependancy to your `mod.conf` and Minetest will automatically install the module for end-users. If you wish to manually install this, then you can either clone the repositiory and add it to your `mods/` folder or download it [here](https://content.minetest.net/packages/benrob0329/table_goodies/).

## Why?
Because Lua needs more atomic modules which add functionality without creating a huge library. This module is supposed to be dead-simple to grab and start using in your project without adding anything extra. One of Lua's strengths is minimalism, and I think that it's ecosystem would do well to mirror that.
