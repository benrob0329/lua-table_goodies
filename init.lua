local PATH = minetest.get_modpath(minetest.get_current_modname()) .. "/"

local success = dofile(PATH .. "table_goodies.lua")
assert(success, "table_goodies failed to load!")
